package main

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"fmt"
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/datastore/postgres"
	"log"
	"os"

	"github.com/joho/godotenv"
	"github.com/kataras/iris/v12"

	"gitlab.com/monolith-buyer-poc/ads/public/config"
	"gitlab.com/monolith-buyer-poc/ads/src/api/router"
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/utils"
	"gitlab.com/monolith-buyer-poc/ads/src/registry"
)

func init() {
	// -- call PrintHeader
	utils.PrintHeader()

	// -- Get Prameters
	_ = godotenv.Load(".env")

	if os.Getenv("MODE_DEBUG") == "" {
		errX := os.Setenv("MODE_DEBUG", config.ModeDebug)
		if errX != nil {
			log.Fatal(errX)
		}
	}

	if os.Getenv("PORT_SERVER") == "" {
		errX := os.Setenv("PORT_SERVER", config.PortServerGo)
		if errX != nil {
			log.Fatal(errX)
		}
	}

	if os.Getenv("LANGUAGE") == "" {
		errX := os.Setenv("LANGUAGE", config.Language)
		if errX != nil {
			log.Fatal(errX)
		}
	}

	if os.Getenv("MAX_SEG_CACHE") == "" {
		errX := os.Setenv("MAX_SEG_CACHE", config.MaxSegCache)
		if errX != nil {
			log.Fatal(errX)
		}
	}

}

func main() {
	// -- get variables
	fmt.Println("****************************************************************************")
	fmt.Println("****************************************************************************")
	fmt.Printf("PORT_SERVER    ==> [%v]\n", os.Getenv("PORT_SERVER"))
	fmt.Printf("LANGUAGE       ==> [%v]\n", os.Getenv("LANGUAGE"))
	fmt.Printf("MODE_DEBUG     ==> [%v]\n", os.Getenv("MODE_DEBUG"))
	fmt.Printf("MAX_SEG_CACHE  ==> [%v]\n", os.Getenv("MAX_SEG_CACHE"))
	fmt.Println("****************************************************************************")
	fmt.Println("****************************************************************************")
	fmt.Printf("Server start: 🛸 [" + os.Getenv("PORT_SERVER") + "]\n")
	fmt.Println("© Yapo.cl 2021")
	fmt.Println("")

	db := postgres.NewDB()

	r := registry.NewRegistry(db)
	app := iris.Default()
	router.NewRouter(app, r.NewAppController())

	// -- Open Port - Server up
	if err := app.Listen(":" + os.Getenv("PORT_SERVER")); err != nil {
		app.Logger().Errorf("error: Unable to start server ")
	}
}
