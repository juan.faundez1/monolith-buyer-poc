package controller

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"gitlab.com/monolith-buyer-poc/ads/src/domain/model"
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/utils"
	"gitlab.com/monolith-buyer-poc/ads/src/usecases/interactor"

	"github.com/kataras/iris/v12"
)

/* Params and Body */
type AdParams struct {
	AdId uint `param:"adId" validate:"min=0"`
}

type adController struct {
	adInteractor interactor.AdInteractor
}

type AdController interface {
	GetAllAds(iris.Context) (int, error)
	GetAdById(iris.Context) (int, error)

	CreateAd(iris.Context) (int, error)
	UpdateAd(iris.Context) (int, error)
	DeleteAd(iris.Context) (int, error)
}

func NewAdsController(us interactor.AdInteractor) AdController {
	return &adController{us}
}

func (cc *adController) DeleteAd(ctx iris.Context) (int, error) {
	var params AdParams
	if err := ctx.ReadParams(&params); err != nil {
		return iris.StatusInternalServerError, err
	}
	
	err := cc.adInteractor.DeleteAd(params.AdId)
	if err != nil {
		return iris.StatusInternalServerError, err
	}

	return iris.StatusOK, err
}

func (cc *adController) UpdateAd(ctx iris.Context) (int, error) {
	var params AdParams
	err := ctx.ReadParams(&params)
	if err != nil {
		return iris.StatusInternalServerError, err
	}

	var ad model.Ad
	err = ctx.ReadJSON(&ad)
	if err != nil {
		return iris.StatusInternalServerError, err
	}
	
	err = cc.adInteractor.UpdateAd(&ad)
	if err != nil {
		return iris.StatusInternalServerError, err
	}

	return iris.StatusOK, err
}

func (cc *adController) CreateAd(ctx iris.Context) (int, error) {
	var ad model.Ad
	err := ctx.ReadJSON(&ad)
	if err != nil {
		return iris.StatusInternalServerError, err
	}

	err = cc.adInteractor.CreateAd(&ad)
	if err != nil {
		return iris.StatusInternalServerError, err
	}

	return iris.StatusOK, err
}

func (cc *adController) GetAdById(ctx iris.Context) (int, error) {
	var params AdParams
	err := ctx.ReadParams(&params)
	if err != nil {
		return iris.StatusInternalServerError, err
	}

	obj, err := cc.adInteractor.GetAdById(params.AdId)
	if err != nil {
		return iris.StatusInternalServerError, err
	}

	if _, err := ctx.JSON(obj); err != nil {
		ctx.Application().Logger().Errorf("error: ctx.JSON => [%v]", err)
		return iris.StatusInternalServerError, err
	}

	var response []byte
	if response, err = utils.JsonMarshall(obj); err != nil {
		ctx.Application().Logger().Errorf("error: utils.JsonMarshall => [%v]", err)
		return iris.StatusInternalServerError, err
	}

	ctx.Application().Logger().Infof("Resultado [%v]", string(response))
	return iris.StatusOK, err
}

func (cc *adController) GetAllAds(ctx iris.Context) (int, error) {
	var obj []model.Ad

	obj, err := cc.adInteractor.GetAllAds()
	if err != nil {
		return iris.StatusInternalServerError, err
	}

	if _, err := ctx.JSON(obj); err != nil {
		ctx.Application().Logger().Errorf("error: ctx.JSON => [%v]", err)
		return iris.StatusInternalServerError, err
	}

	var response []byte
	if response, err = utils.JsonMarshall(obj); err != nil {
		ctx.Application().Logger().Errorf("error: utils.JsonMarshall => [%v]", err)
		return iris.StatusInternalServerError, err
	}

	ctx.Application().Logger().Infof("Resultado [%v]", string(response))
	return iris.StatusOK, err
}
