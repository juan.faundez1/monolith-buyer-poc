package schema

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"errors"

	"github.com/kataras/iris/v12"
	"gitlab.com/monolith-buyer-poc/ads/src/domain/model"
)

// -- https://github.com/kataras/iris/blob/master/_examples/request-body/read-json-struct-validation/main.go
func WrapValidationErrors(ctx iris.Context) (int, error) {
	var p model.AdInputParameters

	// -- Validacion de campo numerico < 0
	if n, err := ctx.Params().GetInt64("adId"); err != nil {
		return iris.StatusBadRequest, err
	} else {
		if n < 0 {
			errorMessage := errors.New("error: parametro recibido menor que 0")
			return iris.StatusBadRequest, errorMessage
		}
	}

	if err := ctx.ReadParams(&p); err != nil {
		return iris.StatusBadRequest, err
	}

	return iris.StatusOK, nil
}
