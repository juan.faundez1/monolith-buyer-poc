package repository

import (
	"gitlab.com/monolith-buyer-poc/ads/src/domain/model"
	"gorm.io/gorm"
)

type adRepository struct {
	db *gorm.DB
}

type AdRepository interface {
	FindAll() ([]model.Ad, error)
	FindById(adId uint) (*model.Ad, error)
}

func NewAdRepository(db *gorm.DB) AdRepository {
	return &adRepository{db}
}

func (ur *adRepository) FindAll() ([]model.Ad, error) {
	var ads []model.Ad
	// TODO: Fix AdParams; Can't no be "joined"
	err := ur.db.
		Preload("AdParams").
		Joins("User").
		Find(&ads).Error

	if err != nil {
		return nil, err
	}

	return ads, nil
}

func (ur *adRepository) FindById(adId uint) (*model.Ad, error) {
	var ad *model.Ad
	// TODO: Fix AdParams; Can't no be "joined"
	err := ur.db.
		Preload("AdParams").
		Joins("User").
		First(&ad, adId).Error

	if err != nil {
		return nil, err
	}

	return ad, nil
}