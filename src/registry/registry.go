package registry

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"gitlab.com/monolith-buyer-poc/ads/src/interface/controller"
	"gorm.io/gorm"
)

/**********************************/
/****** Important Comments ********/
/**********************************/
// -- NewRegistry toma una instancia de base de datos para pasar a la interface/repository.(En caso de requerirse)

type registry struct {
	db *gorm.DB
}

type Registry interface {
	NewAppController() controller.AppController
}

func NewRegistry(db *gorm.DB) Registry {
	return &registry{db}
}

func (r *registry) NewAppController() controller.AppController {
	return r.NewAdController()
}
