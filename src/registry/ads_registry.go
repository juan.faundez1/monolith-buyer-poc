package registry

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/gcp/pubsub"
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/utils/fs"
	"gitlab.com/monolith-buyer-poc/ads/src/interface/controller"
	"gitlab.com/monolith-buyer-poc/ads/src/interface/repository"
	"gitlab.com/monolith-buyer-poc/ads/src/usecases/interactor"
)

/**********************************/
/****** Important Comments ********/
/**********************************/
// -- NewAdsController genera un controlador con interactor.
// -- NewAdsInteractor devuelve un interactor con un repositorio y un presentador.
// -- NewAdsRepository devuelve el repositorio de la interfaz pasada con una instancia
// --    de base de datos que cumple con la interfaz del caso de uso. (En caso de requerirse)

func (r *registry) NewAdController() controller.AdController {
	return controller.NewAdsController(r.NewAdInteractor())
}

func (r *registry) NewAdInteractor() interactor.AdInteractor {
	return interactor.NewAdInteractor(
		r.NewAdRepository(),
		r.NewAdPublisher(),
		r.NewMessageCounter(),
	)
}

func (r *registry) NewAdRepository() repository.AdRepository {
	return repository.NewAdRepository(r.db)
}

func (r *registry) NewAdPublisher() pubsub.AdPublisher {
	return pubsub.NewAdPublisher()
}

func (r *registry) NewMessageCounter() fs.MessageCounter {
	return fs.NewMessageCounter()
}