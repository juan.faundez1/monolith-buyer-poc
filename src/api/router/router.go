package router

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"os"
	"strconv"
	"time"

	"gitlab.com/monolith-buyer-poc/ads/public/config"
	"gitlab.com/monolith-buyer-poc/ads/src/interface/controller"

	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/middleware/logger"
	"github.com/kataras/iris/v12/middleware/recover"

	"github.com/go-playground/validator/v10"
)

func NewRouter(app *iris.Application, c controller.AppController) {
	app.Use(logger.New())
	app.Use(recover.New())
	app.Validator = validator.New()

	// -- configure time.Duration cache
	var nseg int
	var e, f error
	var td time.Duration
	if nseg, e = strconv.Atoi(os.Getenv("MAX_SEG_CACHE")); e != nil {
		if nseg, f = strconv.Atoi(config.MaxSegCache); f != nil {
			td = 1800
		} else {
			td = time.Duration(nseg)
		}
	} else {
		td = time.Duration(nseg)
	}

	// -- configure routeGetAds
	routeAds := app.Party("/buy/poc/v1/ads")
	routeAds.Get("/", iris.Cache(td), func(ctx iris.Context) {
		if httpResponse, err := c.GetAllAds(ctx); err != nil {
			if httpResponse == iris.StatusBadRequest {
				ctx.StopWithStatus(iris.StatusBadRequest)
			} else {
				//ctx.StatusCode(iris.StatusInternalServerError)
				ctx.Application().Logger().Errorf("Error %v", err)
				ctx.StopWithStatus(iris.StatusInternalServerError)
				ctx.Writef("getAdsError: %v", err)
			}
		}
	})

	routeAds.Get("/{adId:uint}", iris.Cache(td), func(ctx iris.Context) {
		if httpResponse, err := c.GetAdById(ctx); err != nil {
			if httpResponse == iris.StatusBadRequest {
				ctx.StopWithStatus(iris.StatusBadRequest)
			} else {
				ctx.Application().Logger().Errorf("Error %v", err)
				ctx.StopWithStatus(iris.StatusInternalServerError)
				ctx.Writef("getAdsError: %v", err)
			}
		}
	})

	routeAds.Post("/", iris.Cache(td), func(ctx iris.Context) {
		if httpResponse, err := c.CreateAd(ctx); err != nil {
			if httpResponse == iris.StatusBadRequest {
				ctx.StopWithStatus(iris.StatusBadRequest)
			} else {
				//ctx.StatusCode(iris.StatusInternalServerError)
				ctx.Application().Logger().Errorf("Error %v", err)
				ctx.StopWithStatus(iris.StatusInternalServerError)
				ctx.Writef("getAdsError: %v", err)
			}
		}
	})

	routeAds.Put("/{adId:uint}", iris.Cache(td), func(ctx iris.Context) {
		if httpResponse, err := c.UpdateAd(ctx); err != nil {
			if httpResponse == iris.StatusBadRequest {
				ctx.StopWithStatus(iris.StatusBadRequest)
			} else {
				//ctx.StatusCode(iris.StatusInternalServerError)
				ctx.Application().Logger().Errorf("Error %v", err)
				ctx.StopWithStatus(iris.StatusInternalServerError)
				ctx.Writef("getAdsError: %v", err)
			}
		}
	})

	routeAds.Delete("/{adId:uint}", iris.Cache(td), func(ctx iris.Context) {
		if httpResponse, err := c.DeleteAd(ctx); err != nil {
			if httpResponse == iris.StatusBadRequest {
				ctx.StopWithStatus(iris.StatusBadRequest)
			} else {
				//ctx.StatusCode(iris.StatusInternalServerError)
				ctx.Application().Logger().Errorf("Error %v", err)
				ctx.StopWithStatus(iris.StatusInternalServerError)
				ctx.Writef("getAdsError: %v", err)
			}
		}
	})

}
