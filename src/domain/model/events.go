package model

import (
	"time"
)

type Message struct {
	Attributes  map[string]string `json:"attributes"`
	Data 	    string            `json:"data"`
	MessageId   string            `json:"messageId"`
	PublishTime time.Time         `json:"publishTime"`
}

type EventPayload struct {
	Message *Message `json:"message"`
}

type AdsOrder struct {
	EventData *EventData `json:"adsOrder"`
}

type EventData struct {
	Operation   string    `json:"operation"`
	AdId        uint      `json:"adId"`
	Title       string    `json:"title"`
	Price       float64   `json:"price"`
	Currency    string    `json:"currency"`
	Category    float64   `json:"category"`
	Description string    `json:"description"`
	Region      float64   `json:"region"`
	Commune     float64   `json:"commune"`
	ListTime    time.Time `json:"listTime"`
	Status      string    `json:"status"`
	UserId      uint      `json:"userId"`
	AdParams    []AdParam `json:"adParams"`
}
