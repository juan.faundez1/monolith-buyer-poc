package model

import (
	"time"
)

// Ad
type AdInputParameters struct {
	AdId int64 `json:"adId"`
}

// Represents an ad entry
type Ad struct {
	// Unique ID of the ad
	AdId uint `json:"adId" example:"101,omitempty" gorm:"primaryKey,column:ad_id"`
	// Title of the ad
	Title string `json:"title" example:"Car ad title" minLength:"1" validate:"required" gorm:"column:title"`
	// Price of the ad
	Price float64 `json:"price" example:"1200" format:"double" validate:"required" gorm:"column:price"`
	// Currency of the ad
	Currency string `json:"currency,omitempty" example:"clp" minLength:"1" enums:"clp,uf" default:"clp" gorm:"column:currency"`
	// Category of the ad
	Category float64 `json:"category" example:"7" format:"int32" validate:"required" gorm:"column:category"`
	// Description text of the ad
	Description string `json:"description" example:"Nice car" minLength:"1" validate:"required" gorm:"column:description"`
	// Region code of the ad location
	Region float64 `json:"region" example:"15" format:"int32" validate:"required" gorm:"column:region"`
	// Commune code of the ad location
	Commune float64 `json:"commune" example:"243" format:"int32" validate:"required" gorm:"column:commune"`
	// Listing Date for the ad
	ListTime time.Time `json:"listTime,omitempty" example:"2021-08-20T20:08:01.810Z" format:"date-time" gorm:"column:list_time"`
	// Ad status
	Status string `json:"status" example:"active" minLength:"1" enums:"inactive,deleted,active,disabled" validate:"required" gorm:"column:status"`

	UserId uint   `json:"-"`
	User   *User  `json:"user" validate:"required" gorm:"foreignKey:UserId;references:UserId"`

	AdParams []AdParam `json:"params" gorm:"foreignKey:AdId;references:AdId"`
}

type User struct {
	// Unique ID of the user
	UserId uint `json:"userId" example:"1" format:"uint" validate:"required" gorm:"primaryKey,column:user_id"`
	// Name of the user
	Name string `json:"name,omitempty" example:"pepito" minLength:"4" maxLength:"50" gorm:"column:name"`
	// Email of the user
	Email string `json:"email,omitempty" example:"pepito@yapo.cl" minLength:"5" maxLength:"50" gorm:"column:email"`
}

type AdParam struct {
	AdParamId uint `gorm:"primarykey,column:ad_param_id"`

	AdId uint `gorm:"column:ad_id"`
	// Parameter id
	Key string `json:"key" example:"geoposition" minLength:"1" maxLength:"50" validate:"required" gorm:"column:key"`
	// Parameter value
	Value string `json:"value" example:"-33.413304, -70.602465" validate:"required" gorm:"column:value"`
}
