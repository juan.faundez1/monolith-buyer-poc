package model

// Standard structure for errors.
type MessageErrorResponse struct {
	// Defined status between the integrating parties
	Status string `json:"status,omitempty" example:"INVALID_ARGUMENT"`
	// User error message.
	UserMessage string `json:"userMessage,omitempty" example:"Unexpected error..."`
	// Internal system error message.
	InternalMessage string `json:"internalMessage,omitempty" example:"Internal error..."`
	// Additional error information.
	MoreInfo string `json:"moreInfo,omitempty" example:"for more information enter https://httpstatuses.com/"`
}
