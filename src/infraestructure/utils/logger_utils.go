package utils

/**********************************/
/******** Import Packages *********/
/**********************************/
import "encoding/json"

func JsonMarshall(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}
