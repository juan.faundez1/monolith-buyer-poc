package fs

import (
	"encoding/binary"
	"os"
)

type messageCounter struct {}

type MessageCounter interface {
	GetAndIncrement() (uint64, error)
}

func NewMessageCounter() MessageCounter {
	return &messageCounter{}
}

func getFile(name string) (*os.File, error) {
	return os.OpenFile(name, os.O_RDWR|os.O_CREATE, 0644)
}

func readCount(file *os.File) (uint64, error) {
	var count uint64
	err := binary.Read(file, binary.LittleEndian, &count)
	return count, err
}

func writeCount(file *os.File, count uint64) error {
	bytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(bytes, count)
	_, err := file.WriteAt(bytes, 0)
	return err
}

func (c *messageCounter)GetAndIncrement() (uint64, error) {
	file, err := getFile("messages_count.bin")
	if err != nil {
		return 0, err
	}
	defer file.Close()

	info, err := file.Stat()
	if err != nil {
		return 0, err
	}

	if info.Size() == 0 {
		err = writeCount(file, 0)
		if err != nil {
			return 0, err
		}
		return 0, nil
	}

	count, err := readCount(file)
	if err != nil {
		return 0, err
	}

	count = count + 1
	err = writeCount(file, count)
	if err != nil {
		return 0, err
	}

	return count, nil
}