package utils

/**********************************/
/******** Import Packages *********/
/**********************************/
import "fmt"

/*
*
* @author   Rodrigo G. Higuera M. <rodrigoghm@gmail.com>
* @date     2021.05.28 18:40:52
* @version  20210528.184052.0001
* @Company  © Development 2021
*
* printHeader() : Funcion que muestra el texto de Yapo.cl Solo cuando inicia el servidor.
*
 */
func PrintHeader() {
	fmt.Println("")
	fmt.Println("")
	fmt.Println("")
	fmt.Println("YYYYYY       YYYYYYY                                                                                   lllllll  ")
	fmt.Println("Y:::::Y       Y:::::Y                                                                                   l:::::l ")
	fmt.Println("Y:::::Y       Y:::::Y                                                                                   l:::::l ")
	fmt.Println("Y::::::Y     Y::::::Y                                                                                   l:::::l ")
	fmt.Println("YYY:::::Y   Y:::::YYY  aaaaaaaaaaaaa   ppppp   ppppppppp      ooooooooooo               cccccccccccccccc l::::l ")
	fmt.Println("   Y:::::Y Y:::::Y     a::::::::::::a  p::::ppp:::::::::p   oo:::::::::::oo           cc:::::::::::::::c l::::l ")
	fmt.Println("    Y:::::Y:::::Y      aaaaaaaaa:::::a p:::::::::::::::::p o:::::::::::::::o         c:::::::::::::::::c l::::l ")
	fmt.Println("     Y:::::::::Y                a::::a pp::::::ppppp::::::po:::::ooooo:::::o        c:::::::cccccc:::::c l::::l ")
	fmt.Println("      Y:::::::Y          aaaaaaa:::::a  p:::::p     p:::::po::::o     o::::o        c::::::c     ccccccc l::::l ")
	fmt.Println("       Y:::::Y         aa::::::::::::a  p:::::p     p:::::po::::o     o::::o        c:::::c              l::::l ")
	fmt.Println("       Y:::::Y        a::::aaaa::::::a  p:::::p     p:::::po::::o     o::::o        c:::::c              l::::l ")
	fmt.Println("       Y:::::Y       a::::a    a:::::a  p:::::p    p::::::po::::o     o::::o        c::::::c     ccccccc l::::l ")
	fmt.Println("       Y:::::Y       a::::a    a:::::a  p:::::ppppp:::::::po:::::ooooo:::::o        c:::::::cccccc:::::cl::::::l")
	fmt.Println("    YYYY:::::YYYY    a:::::aaaa::::::a  p::::::::::::::::p o:::::::::::::::o ......  c:::::::::::::::::cl::::::l")
	fmt.Println("    Y:::::::::::Y     a::::::::::aa:::a p::::::::::::::pp   oo:::::::::::oo  .::::.   cc:::::::::::::::cl::::::l")
	fmt.Println("    YYYYYYYYYYYYY      aaaaaaaaaa  aaaa p::::::pppppppp       ooooooooooo    ......     ccccccccccccccccllllllll")
	fmt.Println("                                        p:::::p                                                                 ")
	fmt.Println("                                        p:::::p                                                                 ")
	fmt.Println("                                       p:::::::p                                                                ")
	fmt.Println("                                       p:::::::p                                                                ")
	fmt.Println("                                       p:::::::p                                                                ")
	fmt.Println("                                       ppppppppp                                                                ")
	fmt.Println("")
	fmt.Println("")
	fmt.Println("")
}
