package pubsub

import (
	"context"
	"fmt"
	"log"

	"cloud.google.com/go/pubsub"
	"gitlab.com/monolith-buyer-poc/ads/src/domain/model"
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/utils"
)

/**********************************/
/*********** Disclaimer ***********/
/**********************************/
// -- Para efecto practico en este primer ejercicio no se implementa nada en el repository

/**********************************/
/******** Import Packages *********/
/**********************************/
// -- Undefined

type adPublisher struct {
}

type AdPublisher interface {
	Publish(event *model.EventPayload) error
}

func NewAdPublisher() AdPublisher {
	return &adPublisher{}
}

// Reminder: export GOOGLE_APPLICATION_CREDENTIALS="/Users/jp/Desktop/Workspace/GCP/Keys/pubsub.json" before running app
func (ap *adPublisher) Publish(payload *model.EventPayload) error {
	ctx := context.Background()
	proj := "lateral-balm-323722" // TODO: Fix naming

	client, err := pubsub.NewClient(ctx, proj)
	if err != nil {
		return err
	}
	defer client.Close()

	topic := client.Topic("FirstTopic") // TODO: Fix naming
	if topic == nil {
		log.Fatalf("Could not get pubsub Topic: FirstTopic")
	}

	var message []byte
	if message, err = utils.JsonMarshall(payload); err != nil {
		return err
	}

	result := topic.Publish(ctx, &pubsub.Message{Data: message})

	// Block until the result is returned and a server-generated
	// ID is returned for the published message.
	id, err := result.Get(ctx)
	if err != nil {
		return err
	}

	fmt.Printf("Published a message; msg ID: %v\n", id)
	return nil
}
