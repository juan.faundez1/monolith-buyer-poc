package repository

/**********************************/
/******** Import Packages *********/
/**********************************/

import (
	"gitlab.com/monolith-buyer-poc/ads/src/domain/model"
)

type AdRepository interface {
	FindAll() ([]model.Ad, error)
	FindById(adId uint) (*model.Ad, error)
}