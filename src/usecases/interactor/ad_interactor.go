package interactor

/**********************************/
/******** Import Packages *********/
/**********************************/
import (
	"gitlab.com/monolith-buyer-poc/ads/src/domain/model"
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/gcp/pubsub"
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/utils"
	"gitlab.com/monolith-buyer-poc/ads/src/infraestructure/utils/fs"
	"gitlab.com/monolith-buyer-poc/ads/src/usecases/repository"
	"strconv"
	"time"
)

type adInteractor struct {
	AdRepository repository.AdRepository
	AdPublisher  pubsub.AdPublisher
	MessageCounter fs.MessageCounter
}

type AdInteractor interface {
	GetAllAds() ([]model.Ad, error)
	GetAdById(adId uint) (*model.Ad, error)
	CreateAd(ad *model.Ad) error
	UpdateAd(ad *model.Ad) error
	DeleteAd(adId uint) error
}

func NewAdInteractor(r repository.AdRepository, p pubsub.AdPublisher, m fs.MessageCounter) AdInteractor {
	return &adInteractor{r, p, m}
}

func (ai *adInteractor) GetAllAds() ([]model.Ad, error) {
	return ai.AdRepository.FindAll()
}

func (ai *adInteractor) GetAdById(adId uint) (*model.Ad, error) {
	return ai.AdRepository.FindById(adId)
}

func (ai *adInteractor) CreateAd(ad *model.Ad) error {
	return ai.publish(createEventData(ad, "POST"))
}

func (ai *adInteractor) UpdateAd(ad *model.Ad) error {
	return ai.publish(createEventData(ad, "PUT"))
}

func (ai *adInteractor) DeleteAd(adId uint) error {
	eventData := &model.EventData{
		Operation: "DELETE",
		AdId:      adId,
	}
	return ai.publish(eventData)
}

func (ai *adInteractor) publish(eventData *model.EventData) error {
	count, err := ai.MessageCounter.GetAndIncrement()
	if err != nil {
		return err
	}

	payload, err := createEventPayload(eventData, strconv.FormatUint(count, 10))
	if err != nil {
		return err
	}

	return ai.AdPublisher.Publish(payload)
}

/* Internal */
func createEventPayload(eventData *model.EventData, messageId string) (*model.EventPayload, error) {
	adsOrder := &model.AdsOrder{EventData: eventData}

	encoded, err := utils.EncodeToBase64(adsOrder)
	if err != nil {
		return nil, err
	}

	return &model.EventPayload{Message: &model.Message{
		Attributes: make(map[string]string),
		Data: encoded,
		MessageId: messageId,
		PublishTime: time.Now(),
	}}, nil
}

func createEventData(ad *model.Ad, operation string) *model.EventData {
	return &model.EventData{
		Operation:   operation,
		AdId:        ad.AdId,
		Title:       ad.Title,
		Price:       ad.Price,
		Currency:    ad.Currency,
		Category:    ad.Category,
		Description: ad.Description,
		Region:      ad.Region,
		Commune:     ad.Commune,
		ListTime:    ad.ListTime,
		Status:      ad.Status,
		UserId:      ad.User.UserId,
		AdParams:    ad.AdParams,
	}
}
