# public/jmetter

Este Directorio se crea con la finalidad guardar pruebas  unitarias realizadas en Apache jMetter.

A veces se clasifica JMeter como herramienta de "generación de carga", pero esto no es una descripción completa de la herramienta. JMeter soporta aserciones para asegurar que los datos recibidos son correctos, por lo que es una herramienta de realización de pruebas automáticas.

```bash
Importante recordar: que en este directorio NO se puede guardar resultados/informes generados por las herramientas, sino los scripts de ejecución de pruebas.

Informes/resultados puede mostrarse en un confluence de la iniciativa desarrollada.
```

## Links Importantes
[Official jMetter Website](http://jmeter.apache.org/)

[jMetter Wiki](https://web.archive.org/web/20111211144709/http://wiki.apache.org/jmeter/JMeterLinks)

[Apache jMetter - Wikipedia](https://en.wikipedia.org/wiki/Apache_JMeter)

## Contribuciones
'Pull requests' son bienvenidos. Para cambios importantes, abra un problema primero para discutir qué le gustaría cambiar.

## Licencia
[GPLv3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Copyleft.svg/220px-Copyleft.svg.png" width="25px" height="25px" style="display:inline;margin:0"> Copyleft | Yapo - 2021

## Autor

Rodrigo G. Higuera M. <<rodrigo.higuera@yapo.cl>> - Team Buyer