# public/postman

Este Directorio se crea con la finalidad guardar 'Collection Postman' de cada proyecto desarrollado, pues permitira crear peticiones sobre APIs de una forma muy sencilla y poder, de esta manera, probar las APIs.

Si bien se pueden crear cuentas y ser administradas no es correcto dejar dicha información en un repositorio por ello, se deja a disponibilidad de quien desee un Collection Postman de los microservicios implementados con sus respectivos metodos HTTP.

## Links Importantes
[Official Postman API Platform Website](https://www.postman.com/)

## Contribuciones
'Pull requests' son bienvenidos. Para cambios importantes, abra un problema primero para discutir qué le gustaría cambiar.

## Licencia
[GPLv3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Copyleft.svg/220px-Copyleft.svg.png" width="25px" height="25px" style="display:inline;margin:0"> Copyleft | Yapo - 2021

## Autor

Rodrigo G. Higuera M. <<rodrigo.higuera@yapo.cl>> - Team Buyer