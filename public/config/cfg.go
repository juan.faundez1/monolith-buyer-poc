/*****************************************************************/
/*****************************************************************/
/*                                                        ,,    **/
/* YMM'   `MM'                                          `7MM    **/
/*  VMA   ,V                                              MM    **/
/*   VMA ,V    ,6"Yb.  `7MMpdMAo.  ,pW"Wq.      ,p6"bo    MM    **/
/*    VMMP    8)   MM    MM   `Wb 6W'   `Wb    6M'  OO    MM    **/
/*     MM      ,pm9MM    MM    M8 8M     M8    8M         MM    **/
/*     MM     8M   MM    MM   ,AP YA.   ,A9 ,, YM.    ,   MM    **/
/*   .JMML.   `Moo9^Yo.  MMbmmd'   `Ybmd9'  db  YMbmd'  .JMML.  **/
/*                       MM                                     **/
/*                     .JMML.                                   **/
/*****************************************************************/
/*****************************************************************/
/*****************************************************************/
/*                            cfg.go                             */
/*                                                               */
/* Descripcion: Variables de configuración solo en caso de no    */
/*              poder leer las variables de entorno.             */
/*                                                               */
/*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
/*  @ Fecha  : 2021.08.28 03:23:00                               */
/*                                                               */
/* © Yapo.cl 2021                                                */
/*****************************************************************/
package config

/*****************************************************/
/**** Definicion de Constantes Globales - Inicio *****/
/*****************************************************/
const (
	ModeDebug    = "0"
	PortServerGo = "8002"
	Language     = "Go"
	MaxSegCache  = "1800"
)

/*****************************************************/
/****** Definicion de Constantes Globales - Fin ******/
/*****************************************************/
