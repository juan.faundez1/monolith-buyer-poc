# public/k6

Este Directorio se crea con la finalidad guardar pruebas unitarias, pruebas de carga, entre otras, realizadas en k6.

```bash
Importante recordar: que en este directorio NO se puede guardar resultados/informes generados por las herramientas, sino los scripts de ejecución de pruebas.

Informes/resultados puede mostrarse en un confluence de la iniciativa desarrollada.
```

## Links Importantes
[Official k6 Website](https://k6.io/)

[Documentación Oficial](https://k6.io/docs/es/)

[k6 - Wikipedia](https://en.wikipedia.org/wiki/K6_(load_testing_tool))

## Contribuciones
'Pull requests' son bienvenidos. Para cambios importantes, abra un problema primero para discutir qué le gustaría cambiar.

## Licencia
[GPLv3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Copyleft.svg/220px-Copyleft.svg.png" width="25px" height="25px" style="display:inline;margin:0"> Copyleft | Yapo - 2021

## Autor

Rodrigo G. Higuera M. <<rodrigo.higuera@yapo.cl>> - Team Buyer