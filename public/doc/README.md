# public/doc

Este Directorio se crea con la finalidad guardar documentación asociada al proyecto. Por ejemplo: Diagramas, imagenes, complementos, etc...

Si bien estamos claros que los repos no son para guardar artefactos binarios, en este directorio se debe guardar hasta las imagenes que se utilizaran en los README para no tener dependencias de terceros.

## Contribuciones
'Pull requests' son bienvenidos. Para cambios importantes, abra un problema primero para discutir qué le gustaría cambiar.

## Licencia
[GPLv3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Copyleft.svg/220px-Copyleft.svg.png" width="25px" height="25px" style="display:inline;margin:0"> Copyleft | Yapo - 2021

## Autor

Rodrigo G. Higuera M. <<rodrigo.higuera@yapo.cl>> - Team Buyer