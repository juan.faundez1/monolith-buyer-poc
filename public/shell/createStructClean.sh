#!/bin/bash

# /*****************************************************************/
# /*****************************************************************/
# /*                                                        ,,    **/
# /* YMM'   `MM'                                          `7MM    **/
# /*  VMA   ,V                                              MM    **/
# /*   VMA ,V    ,6"Yb.  `7MMpdMAo.  ,pW"Wq.      ,p6"bo    MM    **/
# /*    VMMP    8)   MM    MM   `Wb 6W'   `Wb    6M'  OO    MM    **/
# /*     MM      ,pm9MM    MM    M8 8M     M8    8M         MM    **/
# /*     MM     8M   MM    MM   ,AP YA.   ,A9 ,, YM.    ,   MM    **/
# /*   .JMML.   `Moo9^Yo.  MMbmmd'   `Ybmd9'  db  YMbmd'  .JMML.  **/
# /*                       MM                                     **/
# /*                     .JMML.                                   **/
# /*****************************************************************/
# /*****************************************************************/
# /*****************************************************************/
# /*                   createStructClean.go                        */
# /*                                                               */
# /* Descripcion: Crea la estructura principal del                 */
# /*              skeleton/archetype de Go con el framework Iris   */
# /*                                                               */
# /*  @ Autor  : Rodrigo G. Higuera M. <rodrigoghm@gmail.com>      */
# /*  @ Fecha  : 2021.08.28 04:16:00                               */
# /*                                                               */
# /* © Yapo.cl 2021                                                */
# /*****************************************************************/

# /********************************************************************************/
# /*                                CHANGELOG                                     */
# /*                                                                              */
# /* v1.0                                                                         */
# /* -------                                                                      */
# /* - createStructClean.sh: Shell que permitira configurar el proyecto inicial   */
# /*                   bajo clean architecture con el framework de Go Iris.       */
# /*                                                                              */
# /* Autor     : Rodrigo G. Higuera M. (RGHM)                                     */
# /* Fecha ultima Version   : 25/08/2021 11.02                                    */
# /* Version   : 1.0                                                              */
# /* Jira      : NIBUYER-223                                                      */
# /*                                                                              */
# /* Yapo SpA - 2021                                                              */
# /********************************************************************************/
# /********************************************************************************/
# /* Ejecucion:                                                                   */
# /*                                                                              */
# /*   ./createStructClean.sh -h                                                  */
# /*                - Muestra la ayuda de como trabajar con la shell.             */
# /*                                                                              */
# /*   ./createStructClean.sh                                                     */
# /*                - Llamada que permite crear la estructura inicial del proyecto*/
# /*                                                                              */
# /*   ./createStructClean.sh -gomod <nameRepo>                                   */
# /*                - Llamada que permite crear el fichero go.mod del proyecto,   */
# /*                  como segundo parametro se debe enviar el nombre del         */
# /*                  repositorio.                                                */
# /*                                                                              */
# /*   ./createStructClean.sh -goget                                              */
# /*                - Llamada que permite crear el fichero go get de los modulos  */
# /*                  necesarios para el normal funcionamiento del proyecto       */
# /*                                                                              */
# /********************************************************************************/

#/*********************************************************************************/
#/*************************   CONFIGURACION DE PARAMETROS   ***********************/
#/*********************************************************************************/
# -- Parametros de Fechas 
datef="$(date +'%Y%m%d')"
dt=$(date '+%d/%m/%Y');
SubStrD=${dt:0:2}
SubStrM=${dt:3:2}
SubStrY=${dt:6:4}
YearSys=$(date '+%Y')
MonthSys=$(date '+%m')
DaySys=$(date '+%d')
HHSys=$(date '+%H')
MMSys=$(date '+%M')
SSSys=$(date '+%S')
SubStrDate="${SubStrY}${SubStrM}${SubStrD}"
SubStrDateAAAAMM="${SubStrY}${SubStrM}"
FormatDateMail="${YearSys}.${MonthSys}.${DaySys}"
FormatHeader="${YearSys}/${MonthSys}/${DaySys} ${HHSys}:${MMSys}:${SSSys}"


# -- Rutas Generales del Script
PATH_LOGS_SHELL="./public/log/"
PATHDIR_GKE="./GKE/"
PATHDIR_PUBLIC="./public/"
PATHDIR_PUBLIC_CONFIG="${PATHDIR_PUBLIC}config/"
PATHDIR_PUBLIC_DOC="${PATHDIR_PUBLIC}doc/"
PATHDIR_PUBLIC_JMETTER="${PATHDIR_PUBLIC}jmetter/"
PATHDIR_PUBLIC_K6="${PATHDIR_PUBLIC}k6/"
PATHDIR_PUBLIC_LOG="${PATHDIR_PUBLIC}log/"
PATHDIR_PUBLIC_POSTMAN="${PATHDIR_PUBLIC}postman/"
PATHDIR_PUBLIC_SHELL="${PATHDIR_PUBLIC}shell/"
PATHDIR_PUBLIC_TMP="${PATHDIR_PUBLIC}tmp/"
PATHDIR_SRC="./src/"
PATHDIR_SRC_ROUTER="${PATHDIR_SRC}api/router"
PATHDIR_SRC_MODEL="${PATHDIR_SRC}domain/model"
PATHDIR_SRC_GCP="${PATHDIR_SRC}infraestructure/gcp"
PATHDIR_SRC_GCP_DATASTORE="${PATHDIR_SRC_GCP}/datastore"
PATHDIR_SRC_GCP_PUBSUB="${PATHDIR_SRC_GCP}/pubsub"
PATHDIR_SRC_LOGGER="${PATHDIR_SRC}infraestructure/logger"
PATHDIR_SRC_PG="${PATHDIR_SRC}infraestructure/pg"
PATHDIR_SRC_UTILS="${PATHDIR_SRC}infraestructure/utils"
PATHDIR_SRC_IF_CONTROLLER="${PATHDIR_SRC}interface/controller"
PATHDIR_SRC_IF_PRESENTER="${PATHDIR_SRC}interface/presenter"
PATHDIR_SRC_IF_REPOSITORY="${PATHDIR_SRC}interface/repository"
PATHDIR_SRC_IF_SCHEMA="${PATHDIR_SRC}interface/schema"
PATHDIR_SRC_REGISTRY="${PATHDIR_SRC}registry"
PATHDIR_SRC_TEST="${PATHDIR_SRC}tests"
PATHDIR_SRC_TEST_MOCKS="${PATHDIR_SRC_TEST}/mocks"
PATHDIR_SRC_UC_INTERACTOR="${PATHDIR_SRC}usecases/interactor"
PATHDIR_SRC_UC_PRESENTER="${PATHDIR_SRC}usecases/presenter"
PATHDIR_SRC_UC_REPOSITORY="${PATHDIR_SRC}usecases/repository"
PATHDIR_BIN="./bin/"

# -- Nombres genericos de archivos
nameLogsShell="createStructClean.log"
srcLogsShell="${PATH_LOGS_SHELL}${nameLogsShell}"

# -- Variables Genericas
FLG_DEBUG=0
File="$(basename $0)"
FLAG_LINUX=0
FLAG_MAC=0
opc_goMod=0
opc_goGet=0
max_lenght_field=99
#/*********************************************************************************/

# -- Escritura de Log de la shell
WriteLog()
{
	logLevel=$1
	funcion=$2
	linea=$3
	strTxt=$4
	if [ ${FLG_DEBUG} -eq 1 ]; then
		if [ ${FLAG_MAC} -eq 1 ]; then
			echo -e "\x1B[36mWrite Log... => \x1B[0m ${strTxt}"
		fi
		if [ ${FLAG_LINUX} -eq 1 ]; then
			echo -e "\e[36mWrite Log... => \e[0m ${strTxt}"
		fi
	fi
	
	echo "logdate=\"$(date +'%Y-%m-%d') $(date +'%H:%M:%S')\", log_level=\"${logLevel}\", file=\"${File}\", func=\"${funcion}\", line=\"${linea}\", msj=\"${strTxt}\" "  >> ${srcLogsShell}
}

# -- Funcion que muestra el uso de la shell
MostrarAyudaLinux()
{
	echo ""
	echo -e "\e[36m::: Forma de Ejecutar Shell createStructClean.sh :::\e[0m"
	echo -e "\e[36m====================================================\e[0m"
	echo ""
	echo -e "\e[5mOpciones\e[0m"
	echo -e "\e[5m----------\e[0m"
	echo ""
	echo -e "\e[5m-h :\e[0m Help. Permite visualizar el modo de emplear esta shell."
	echo ""
	echo -e "\e[4mEjemplo :\e[0m"
	echo ""
	echo "./createStructClean.sh -h"
	echo ""
	echo ""		
	echo -e "\e[5m-goget :\e[0m Esta opcion realiza todos los go get necesarios del proyecto."
	echo ""
	echo -e "\e[4mEjemplo :\e[0m"
	echo ""
	echo "./createStructClean.sh -goget"
	echo ""
	echo ""
	echo -e "\e[5m-gomod <nameRepo> :\e[0m Esta opcion realiza el 'go mod' correspondiente indicando en un segundo parametro el nombre del repo"
	echo ""
	echo -e "\e[4mEjemplo :\e[0m"
	echo ""
	echo "./createStructClean.sh -gomod gitlab.com/buy/test"
	echo ""
	echo ""
}

# -- Funcion que muestra el uso de la shell
MostrarAyudaMac()
{
	echo ""
	echo -e "\x1B[36m::: Forma de Ejecutar Shell createStructClean.sh :::\x1B[0m"
	echo -e "\x1B[36m====================================================\x1B[0m"
	echo ""
	echo -e "\x1B[5mOpciones\x1B[0m"
	echo -e "\x1B[5m----------\x1B[0m"
	echo ""
	echo -e "\x1B[5m-h :\x1B[0m Help. Permite visualizar el modo de emplear esta shell."
	echo ""
	echo -e "\x1B[4mEjemplo :\x1B[0m"
	echo ""
	echo "./createStructClean.sh -h"
	echo ""
	echo ""		
	echo -e "\x1B[5m-goget :\x1B[0m Esta opcion realiza todos los go get necesarios del proyecto."
	echo ""
	echo -e "\x1B[4mEjemplo :\x1B[0m"
	echo ""
	echo "./createStructClean.sh -goget"
	echo ""
	echo ""
	echo -e "\x1B[5m-gomod <nameRepo> :\x1B[0m Esta opcion realiza el 'go mod' correspondiente indicando en un segundo parametro el nombre del repo"
	echo ""
	echo -e "\x1B[4mEjemplo :\x1B[0m"
	echo ""
	echo "./createStructClean.sh -gomod gitlab.com/buy/test"
	echo ""
	echo ""
}

# -- Funcion que completa con puntos un string
completaEspacios()
{
	varInput=`echo "$1" | sed -r 's/[_]+/ /g'`
    lenght_string=0
	lenght_string=`echo -n "${varInput}" | wc -c`
	car="."
	str=""
	index=""
	while [ ${lenght_string} -le ${max_lenght_field} ]; do
		str="${str}${car}"
		let lenght_string+=1
	done
	index="${varInput}"${str}
}

codeResponse()
{
	resp=$1
	dir=$2
	opc=$3

	if [ ${opc} -eq 1 ]; then
		str1="Directory_Creation_[${dir}]"
	fi

	if [ ${opc} -eq 2 ]; then
		str1="Go_mod_run"
	fi

	if [ ${opc} -eq 3 ]; then
		str1="Go_get_run_[${dir}]"
	fi
	
	completaEspacios ${str1}
	if [ ${resp} -eq 0 ]; then
		if [ ${FLAG_MAC} -eq 1 ]; then
			echo -e "${index} \x1B[32m[ OK ]\x1B[0m"
			WriteLog "LOG" "codeResponse()" ${LINENO} "${index} [ OK ]"
		fi
		if [ ${FLAG_LINUX} -eq 1 ]; then
			echo -e "${index} \e[32m[ OK ]\e[0m"
			WriteLog "LOG" "codeResponse()" ${LINENO} "${index} [ OK ]"
		fi
	else
		if [ ${FLAG_MAC} -eq 1 ]; then
			echo -e "${index} \x1B[31m[ FALLO ]\x1B[0m"
			WriteLog "ERROR" "codeResponse()" ${LINENO} "${index} [ FALLO ]"
		fi
		if [ ${FLAG_LINUX} -eq 1 ]; then
			echo -e "${index} \e[31m[ FALLO ]\e[0m"
			WriteLog "ERROR" "codeResponse()" ${LINENO} "${index} [ FALLO ]"
		fi
	fi
}

CreacionDirectoriosCleanArchitecture()
{
	mkdir -p ${PATHDIR_GKE}
	codeResponse $? ${PATHDIR_GKE} "1"
	
	mkdir -p ${PATHDIR_PUBLIC}
	codeResponse $? ${PATHDIR_PUBLIC} "1"

	mkdir -p ${PATHDIR_PUBLIC_CONFIG}
	codeResponse $? ${PATHDIR_PUBLIC_CONFIG} "1"

	mkdir -p ${PATHDIR_PUBLIC_DOC}
	codeResponse $? ${PATHDIR_PUBLIC_DOC} "1"

	mkdir -p ${PATHDIR_PUBLIC_JMETTER}
	codeResponse $? ${PATHDIR_PUBLIC_JMETTER} "1"

	mkdir -p ${PATHDIR_PUBLIC_K6}
	codeResponse $? ${PATHDIR_PUBLIC_K6} "1"

	mkdir -p ${PATHDIR_PUBLIC_LOG}
	codeResponse $? ${PATHDIR_PUBLIC_LOG} "1"

	mkdir -p ${PATHDIR_PUBLIC_POSTMAN}
	codeResponse $? ${PATHDIR_PUBLIC_POSTMAN} "1"

	mkdir -p ${PATHDIR_PUBLIC_SHELL}
	codeResponse $? ${PATHDIR_PUBLIC_SHELL} "1"

	mkdir -p ${PATHDIR_PUBLIC_TMP}
	codeResponse $? ${PATHDIR_PUBLIC_TMP} "1"
	
	mkdir -p ${PATHDIR_SRC_ROUTER}
	codeResponse $? ${PATHDIR_SRC_ROUTER} "1"

	mkdir -p ${PATHDIR_SRC_MODEL}
	codeResponse $? ${PATHDIR_SRC_MODEL} "1"

	mkdir -p ${PATHDIR_SRC_GCP}
	codeResponse $? ${PATHDIR_SRC_GCP} "1"

	mkdir -p ${PATHDIR_SRC_GCP_DATASTORE}
	codeResponse $? ${PATHDIR_SRC_GCP_DATASTORE} "1"

	mkdir -p ${PATHDIR_SRC_GCP_PUBSUB}
	codeResponse $? ${PATHDIR_SRC_GCP_PUBSUB} "1"

	mkdir -p ${PATHDIR_SRC_LOGGER}
	codeResponse $? ${PATHDIR_SRC_LOGGER} "1"

	mkdir -p ${PATHDIR_SRC_PG}
	codeResponse $? ${PATHDIR_SRC_PG} "1"

	mkdir -p ${PATHDIR_SRC_UTILS}
	codeResponse $? ${PATHDIR_SRC_UTILS} "1"

	mkdir -p ${PATHDIR_SRC_IF_CONTROLLER}
	codeResponse $? ${PATHDIR_SRC_IF_CONTROLLER} "1"

	mkdir -p ${PATHDIR_SRC_IF_PRESENTER}
	codeResponse $? ${PATHDIR_SRC_IF_PRESENTER} "1"
	
	mkdir -p ${PATHDIR_SRC_IF_REPOSITORY}
	codeResponse $? ${PATHDIR_SRC_IF_REPOSITORY} "1"

	mkdir -p ${PATHDIR_SRC_IF_SCHEMA}
	codeResponse $? ${PATHDIR_SRC_IF_SCHEMA} "1"

	mkdir -p ${PATHDIR_SRC_REGISTRY}
	codeResponse $? ${PATHDIR_SRC_REGISTRY} "1"

	mkdir -p ${PATHDIR_SRC_TEST}
	codeResponse $? ${PATHDIR_SRC_TEST} "1"

	mkdir -p ${PATHDIR_SRC_TEST_MOCKS}
	codeResponse $? ${PATHDIR_SRC_TEST_MOCKS} "1"

	mkdir -p ${PATHDIR_SRC_UC_INTERACTOR}
	codeResponse $? ${PATHDIR_SRC_UC_INTERACTOR} "1"

	mkdir -p ${PATHDIR_SRC_UC_PRESENTER}
	codeResponse $? ${PATHDIR_SRC_UC_PRESENTER} "1"

	mkdir -p ${PATHDIR_SRC_UC_REPOSITORY}
	codeResponse $? ${PATHDIR_SRC_UC_REPOSITORY} "1"

	mkdir -p ${PATHDIR_BIN}
	codeResponse $? ${PATHDIR_BIN} "1"
}

#/*********************************************************************************/
#/********************************   INICIO   *************************************/
#/*********************************************************************************/

mkdir -p ${PATHDIR_PUBLIC_LOG}

# -- Identificacion de SSOO
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
	FLAG_LINUX=1
elif [[ "$OSTYPE" == "darwin"* ]]; then
	FLAG_MAC=1
fi

WriteLog "INFO" "main()" ${LINENO} ":::::::::::::::::::::::::::::::::::::"
WriteLog "INFO" "main()" ${LINENO} "Inicio script"

if [ $# -eq 0 ]; then
	# -- Se crean directorios principales - Clean Architecture
	CreacionDirectoriosCleanArchitecture
else
	if [ $# -eq 1 ]; then
		if [[ $1 == "-h" ]]; then
			if [ ${FLAG_MAC} -eq 1 ]; then
				MostrarAyudaMac
			fi
			if [ ${FLAG_LINUX} -eq 1 ]; then
				MostrarAyudaLinux
			fi
		else
			if [[ $1 == "-goget" ]]; then
				# -- Se ejecuta go get basico de Iris
				# while true; do
					# echo ""
					# read -p "Desea realizar el go get basico de Iris? [Y/N] => " yn
					# if [ ${FLAG_MAC} -eq 1 ]; then
						# case $yn in
							# [Yy]* ) WriteLog "LOG" "main()" ${LINENO} "Se ha de realizar el 'go get'"; opc_goGet=1; break;;
							# [Nn]* ) WriteLog "LOG" "main()" ${LINENO} "Cancelando opcion de ejecutar el 'go get'"; opc_goGet=0; break;;
							# * ) echo -e "\x1B[41m<ERROR> ::: Por favor, responda si[Y] o no[N]. ::: <ERROR>\x1B[0m"
							# echo  ;
						# esac
					# fi
					# if [ ${FLAG_LINUX} -eq 1 ]; then
						# case $yn in
						# 	[Yy]* ) WriteLog "LOG" "main()" ${LINENO} "Se ha de realizar el 'go get'"; opc_goGet=1; break;;
						# 		[Nn]* ) WriteLog "LOG" "main()" ${LINENO} "Cancelando opcion de ejecutar el 'go get'"; opc_goGet=0; break;;
						# 	* ) echo -e "\e[41m<ERROR> ::: Por favor, responda si[Y] o no[N]. ::: <ERROR>\e[0m"
						# 	echo  ;
						# esac
					# fi
					
				# done

				opc_goGet=1
				# -- Execute go get
				if [ ${opc_goGet} -eq 1 ]; then
					go get github.com/kataras/iris/v12@master
					codeResponse $? "Iris" "3"
					go get github.com/kataras/iris/v12/httptest
					codeResponse $? "httptest" "3"
					go get github.com/stretchr/testify
					codeResponse $? "testify" "3"
					go get github.com/joho/godotenv
					codeResponse $? "godotenv" "3"
				fi
			else
				echo ""
				echo ""
				if [ ${FLAG_LINUX} -eq 1 ]; then
					echo -e "\e[41m::: [ERROR] Opcion Invalida [ERROR] :::\e[0m"
				fi
				if [ ${FLAG_MAC} -eq 1 ]; then
					echo -e "\x1B[41m::: [ERROR] Opcion Invalida [ERROR] :::\x1B[0m"
				fi
				echo ""
				echo ""
				# -- Salida controlada
				exit 1
			fi
		fi
	else
		if [ $# -eq 2 ]; then
			if [[ $1 == "-gomod" ]]; then
				# -- Se crea el go mod
				# while true; do
					# echo ""
					# read -p "Desea crear el go.mod? [Y/N] => " yn
					# if [ ${FLAG_MAC} -eq 1 ]; then
						# case $yn in
						# 	[Yy]* ) WriteLog "LOG" "main()" ${LINENO} "Se ha de realizar el  'go.mod'"; opc_goMod=1; break;;
						# 	[Nn]* ) WriteLog "LOG" "main()" ${LINENO} "Cancelando opcion de ejecutar el 'go.mod'"; opc_goMod=0; break;;
						# 	* ) echo -e "\x1B[41m<ERROR> ::: Por favor, responda si[Y] o no[N]. ::: <ERROR>\x1B[0m"
						# 	echo  ;
						# esac
					# fi

					# if [ ${FLAG_LINUX} -eq 1 ]; then
						# case $yn in
						# 	[Yy]* ) WriteLog "LOG" "main()" ${LINENO} "Se ha de realizar el  'go.mod'"; opc_goMod=1; break;;
						# 		[Nn]* ) WriteLog "LOG" "main()" ${LINENO} "Cancelando opcion de ejecutar el 'go.mod'"; opc_goMod=0; break;;
						# 	* ) echo -e "\e[41m<ERROR> ::: Por favor, responda si[Y] o no[N]. ::: <ERROR>\e[0m"
						# 	echo  ;
						# esac
					# fi
				# done

				# -- Execute go mod
				opc_goMod=1
				if [ ${opc_goMod} -eq 1 ]; then
				# 	echo ""
				# 	read -p "Introduzca el nombre del proyecto/repositorio => " nameGoMod
				go mod init $2 2>>${srcLogsShell}
				codeResponse $? "" "2"
				fi
			else
				echo ""
				echo ""
				if [ ${FLAG_LINUX} -eq 1 ]; then
					echo -e "\e[41m::: [ERROR] Opcion Invalida [ERROR] :::\e[0m"
				fi
				if [ ${FLAG_MAC} -eq 1 ]; then
					echo -e "\x1B[41m::: [ERROR] Opcion Invalida [ERROR] :::\x1B[0m"
				fi
				echo ""
				echo ""
				# -- Salida controlada
				exit 1
			fi
		fi	
	fi
fi

# -- Se crea fichero de version de Go.
if [ -e go.mod ]; then
	cat go.mod | grep "go 1." > ./GoVERSION
fi

# -- Se crea fichero de version de Iris.
if [ -e go.mod ]; then
	cat go.mod | grep "kataras/iris" | awk '{print $2}' > ./IrisVERSION
fi

WriteLog "INFO" "main()" ${LINENO} "Se liberan variables declaradas"
WriteLog "INFO" "main()" ${LINENO} "Salida controlada - exit(0)"
WriteLog "INFO" "main()" ${LINENO} "Fin script"

# -- Se liberan las variables declaradas
unset datef
unset dt
unset SubStrD
unset SubStrM
unset SubStrY
unset YearSys
unset MonthSys
unset DaySys
unset HSys
unset MMSys
unset SSSys
unset ubStrDate
unset SubStrDateAAAAMM
unset FormatDateMail
unset FormatHeader
unset PATH_LOGS_SHELL
unset PATHDIR_GKE
unset PATHDIR_PUBLIC
unset PATHDIR_PUBLIC_CONFIG
unset PATHDIR_PUBLIC_DOC
unset PATHDIR_PUBLIC_JMETTER
unset PATHDIR_PUBLIC_K6
unset PATHDIR_PUBLIC_LOG
unset PATHDIR_PUBLIC_POSTMAN
unset PATHDIR_PUBLIC_SHELL
unset PATHDIR_PUBLIC_TMP
unset PATHDIR_SRC
unset PATHDIR_SRC_ROUTER
unset PATHDIR_SRC_MODEL
unset PATHDIR_SRC_GCP
unset PATHDIR_SRC_GCP_DATASTORE
unset PATHDIR_SRC_GCP_PUBSUB
unset PATHDIR_SRC_LOGGER
unset PATHDIR_SRC_PG
unset PATHDIR_SRC_UTILS
unset PATHDIR_SRC_IF_CONTROLLER
unset PATHDIR_SRC_IF_PRESENTER
unset PATHDIR_SRC_IF_REPOSITORY
unset PATHDIR_SRC_IF_SCHEMA
unset PATHDIR_SRC_REGISTRY
unset PATHDIR_SRC_TEST
unset PATHDIR_SRC_TEST_MOCKS
unset PATHDIR_SRC_UC_INTERACTOR
unset PATHDIR_SRC_UC_PRESENTER
unset PATHDIR_SRC_UC_REPOSITORY
unset nameLogsShell
unset srcLogsShell
unset FLG_DEBUG
unset File
unset FLAG_LINUX
unset FLAG_MAC
unset opc_goMod
unset opc_goGet

# -- Salida exitosa del script
exit 0

#/*********************************************************************************/
#/**********************************  FIN  ****************************************/
#/*********************************************************************************/