module gitlab.com/monolith-buyer-poc/ads

go 1.16

require (
	cloud.google.com/go/pubsub v1.16.0
	github.com/Joker/hpp v1.0.0 // indirect
	github.com/alexbrainman/sspi v0.0.0-20180613141037-e580b900e9f5 // indirect
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/jcmturner/gokrb5/v8 v8.2.0 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/kataras/iris/v12 v12.2.0-alpha2.0.20210717090056-b2cc3a287149
	github.com/kr/pretty v0.3.0 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/smartystreets/goconvey v1.6.4 // indirect
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/yudai/pp v2.0.1+incompatible // indirect
	golang.org/x/crypto v0.0.0-20210915214749-c084706c2272 // indirect
	golang.org/x/sys v0.0.0-20210806184541-e5e7981a1069 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/jcmturner/aescts.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/dnsutils.v1 v1.0.1 // indirect
	gopkg.in/jcmturner/goidentity.v3 v3.0.0 // indirect
	gopkg.in/jcmturner/gokrb5.v7 v7.5.0 // indirect
	gopkg.in/jcmturner/rpc.v1 v1.1.0 // indirect
	gorm.io/driver/postgres v1.1.1 // indirect
	gorm.io/gorm v1.21.15 // indirect
)
