# Go Clean Architecture

## Tabla de Contenido
- [Requerimientos a resolver](#requerimiento-a-resolver-api-build)
- [Descripción de la solución](#descripción-de-la-solución)
- [Health check API endpoint](#health-check-api-endpoint)
- [Stack Tecnológico](#stack-tecnológico)
- [Estructura de Directorios](#estructura-de-directorios)
- [Configuración de Variables de Ambiente (Environment Variables)](#configuración-de-variables-de-ambiente-environment-variables)
- [Instalación de la API en Ambiente Local Dockerizado](#instalación-de-la-api-en-ambiente-local-dockerizado)
- [Compilaciones Cross-Platform de Go](#compilaciones-cross-platform-de-go)
- [Testing de la API](#testing-de-la-api)
- [Enlaces Importantes](#enlaces-importantes)
- [Licencia](#licencia)
- [Autor](#autor)

## Requerimiento a Resolver - API Build
1.- Crear una API que implemente el método HTTP GET que reciba un parámetro de entrada (un entero positivo) y la misma se encargue de obtener el crapulo y el número de iteraciones para poder encontrar el crapulo del número ingresado.

Se considera el crapulo a todo número entero positivo entre 0 y 9 que sea el resultado de las sumas reiteradas de las cifras que componen un número hasta que el mismo sea < 10. Por ejemplo, se explicará detalladamente el cálculo del siguiente crapulo:

Numero|Nro. de Iteración|Suma Cifras|Crapulo
---|---|---|---|
1456499484867|1|75|NO|
75|2|12|NO|
12|3|3|SI|

Valor del JSON a Devolver:
```json
{
    "number": 1456499484867,
    "crapulo": 3,
    "iterations": 3
}
```
Otros Ejemplos de Crápulos:

Número|Crápulo|Iteraciones
---|---|---|
145649|2|3|
134|8|1|
9|9|0|
1244|2|2|

## Descripción de la Solución
El requerimiento hace referencia a la construcción de una API bajo un skeleton/archetype basado en Clean Architecture, que dado un determinado número (enviado como parámetro) servirá de entrada para el cálculo del número crapulo de la cifra ingresada. Para resolver el problema se implementó el siguiente método:

- GetCrapuloNumber : Esta API emplea el método http GET para devolver el crapulo del número ingresado. Los valores que devolverá dicha API son una notación de JavaScript Object Notation. (JSON):

    - number  : Le devuelve al usuario el número ingresado.
    - crapulo : El número calculado considerado como crapulo.
    - iterations : Número de veces que se tuvo que iterar en la sumatoria de las cifras para poder obtener el crapulo.

EL caso de uso implementado como parte de la interfaz 'CrapuloInteractor' se resuelve fundamentalmente con el siguiente codigo del script [src/usecases/interactor/crapulo_interactor.go](https://gitlab.com/yapo_team/yotf/buyers/ms-buy-bck-go-clean-architecture/-/blob/main/src/usecases/interactor/crapulo_interactor.go):
```go
func (v *crapuloInteractor) Get(num int64) (model.CrapuloStruct, error) {
	r1, r2 := v.algorithmCrapuloNumber(num)
	return v.CrapuloPresenter.ResponseCrapuloNumber(num, r1, r2), nil
}

func (v *crapuloInteractor) algorithmCrapuloNumber(num int64) (int64, int) {
	result := int(num)
	i := 0
	for result >= 10 {
		i++
		result = v.digitSumAlgorithm(result)
	}

	return int64(result), i
}

func (v *crapuloInteractor) digitSumAlgorithm(num int) int {
	if num > 0 {
		return (num % 10) + v.digitSumAlgorithm(num/10)
	}
	return 0
}
```

## Health check API endpoint
El microservicio desarrollado, tiene un endpoint destinado a obtener la salud o verificación del estado del microservicio. El mismo se encuentra implementado con el método:

- GetHealthCheck

## Stack Tecnológico
- La implementación del desarrollo se llevo a cabo con el lenguaje de programación [Go](https://golang.org/), en su versión 1.16 y con la implementación de módulos para gestión de paquetes. (go mod)
- La implementación contempla el uso de Go [Framework Iris](https://www.iris-go.com/) en su versión número 12.
- De manera nativa se implementó el enrutador(multiplexer) y dispatcher del [Framework Iris](https://www.iris-go.com/)
- La gestión de Testing para los handlers se realizó con los adaptadores ofrecidos por el [Framework Iris](https://www.iris-go.com/)
- La gestión de Testing para los casos de uso se implementó a través de la libreria [testify](https://github.com/stretchr/testify)
- Se importó la librería [godotenv](https://github.com/joho/godotenv) para el manejo de variables de entornos. Sea desde un fichero .env(local) o desde valores definidos en un pipeline.
- Por el API Versioning ni nos preocupamos, Iris proporciona un excelente control de versiones de semver para API.
- Se importa librería [logrus](https://github.com/sirupsen/logrus) que es un logger estructurado para Golang, compatible totalmente con la librería nativa logger.
- Queda pendiente la implementación del ORM [GORM](https://gorm.io/index.html)
- Para StressTest se implementara K6.
- Para LoadTesting se implementara Apache jMetter y K6.

## Estructura de Directorios
Los directorios del desarrollo de la librería se organizan de la siguiente manera:

**GKE       :** Directorio necesario que contiene los archivos de deployment, configmap y autoscaling del aplicativo en el provider Google Cloud Platform.

**public    :** Directorio de funcionalidades anexas, configuración, documentación, scripts de pruebas de cargas y de stress, ficheros logs de respaldo, proyectos postman así como de shell scripting necesarios para el proceso build del proyecto.

**public/config  :** Este directorio se crea con la finalidad guardar configuraciones locales en variables de Go. Las mismas sólo se necesitaran en caso de tener problemas con la lectura de parámetros (variables de entorno)

**public/doc     :** Este directorio se crea con la finalidad de guardar documentación asociada al proyecto. Por ejemplo: Diagramas, imágenes, complementos, etc...

Si bien estamos claros que los repos no son para guardar artefactos binarios, en este directorio se debe guardar hasta las imágenes que se utilizaran en los README para no tener dependencias de terceros.

**public/jmetter :** Este directorio se crea con la finalidad guardar pruebas  unitarias realizadas en Apache jMetter.

**public/k6      :** Este directorio se crea con la finalidad guardar pruebas unitarias, pruebas de carga, entre otras, realizadas en k6.

**public/log     :** Este directorio se crea con la finalidad de almacenar logs del aplicativo, por ejemplo los generados por la shell 'public/shell/createStructClean.sh' o cualquier log que se considere necesario almacenar.

**public/postman :** Este Directorio se crea con la finalidad guardar 'Collection Postman' de cada proyecto desarrollado, pues permitirá crear peticiones sobre APIs de una forma muy sencilla y poder, de esta manera, probar las APIs.

Si bien se pueden crear cuentas y ser administradas no es correcto dejar dicha información en un repositorio por ello, se deja a disponibilidad de quien desee un Collection Postman de los microservicios implementados con sus respectivos metodos HTTP.

**public/shell   :** Este directorio se crea con la finalidad de almacenar shell scripting requeridas para tareas de build del proyecto.

**public/tmp     :** Este directorio se crea con la finalidad de almacenar información temporal de la ejecución del microservicio.

**src       :**  Directorio creado y necesario donde se desarrolla la lógica de negocio de la API a implementar así como las diferentes capas que constituyen el Clean Architecture del proyecto.

**src/api/router      :** Implementación del enrutador(multiplexer) y dispatcher del framework Iris.

**src/domain/model    :** Directorio que gestiona las estructuras de las abstracciones a implementar. 

**src/infraestructure :** Directorio que gestiona toda entrada/salida del aplicativo e implementa cualquier dependencia externa del microservicio. Por ejemplo: Bases de Datos, Gestión de tópicos, UI, Drivers, etc...

**src/interface       :** Este directorio maneja la implementación del "Interface Adapter Handles". Es importante conocer, que en las Interfaces se tienen los "controllers" (Que maneja la implementación del puerto de entrada de los casos de uso) y los "Presenters" (Que maneja la implementación del puerto de salida de los casos de uso).

De igual manera las Interfaces manejan las validaciones de los schemas del modelo de datos.

**src/registry        :** Directorio que maneja la resolución de dependencias usando Inyección de constructores.

**src/test/mocks      :** Directorio que maneja la implementación de los mocks de pruebas unitarias.

**src/usecases        :** Directorio que contiene las reglas de negocio de aplicaciones que utilizan un modelo de dominio y tienen puerto de entrada y de salida.

## Configuración de Variables de Ambiente (Environment Variables)
En su ambiente local de desarrollo la configuración de las variables de entorno se pueden hacer o bien a través del fichero [.env](https://gitlab.com/yapo_team/yotf/buyers/ms-buy-bck-go-clean-architecture/-/blob/main/.env) o directamente editando el fichero [public/config/cfg.go](https://gitlab.com/yapo_team/yotf/buyers/ms-buy-bck-go-clean-architecture/-/blob/main/public/config/cfg.go)

A nivel ya de un ambiente o ecosistema NO local las variables de entorno serán configuradas en GitLab, por ello es necesario especificar cuáles son las variables requeridas para el deploy del proyecto en GCP, a continuación se deja una guía de las variables que forman parte del pipeline de despliegue, se pueden mencionar:

Variable|Tipo de dato en Go|Observación
---|---|---|
DOCKER_AUTH_CONFIG|String|Configuración de Docker para autenticación|
GCLOUD_CLUSTER|String|Cluster k8s de GCP para el Proyecto|
GCLOUD_PROJECT_ID|String|Nombre del proyecto de GCP|
GCLOUD_ZONE|String|Zone en la cual está configurada en cluster GCP|
GCP_REGISTRY_CONFIG|String|System Account del Registry de GCP|
K8S_NAMESPACE_DEV |String|Namespace de desarrollo para deployar|
K8S_NAMESPACE_PROD|String|Namespace de producción para deployar|
KUBE_SA_GCP_CONFIG|String|System Account del Kubernetes Admin de GCP|
LANGUAGE|String|Lenguaje de Desarrollo(stack tecnológico) en que esta la implementación|
MODE_DEBUG_DEV|String|Flag que activa/desactiva el Debug del aplicativo en ambiente de desarrollo|
MODE_DEBUG_PROD|String|Flag que activa/desactiva el Debug del aplicativo en ambiente productivo|
PORT_SERVER_DEV|String|Puerto en que esta configurado el server en ambiente de desarrollo|
PORT_SERVER_PROD|String|Puerto en que esta configurado el server en ambiente de desarrollo|
PROJECT_NAME|String|Nombre del proyecto GCP|
MAX_SEG_CACHE_DEV|String|Maxima cantidad de Segundo con que se habilita la cache en ambiente de Desarrollo|
MAX_SEG_CACHE_PROD|String|Maxima cantidad de Segundo con que se habilita la cache en ambiente de Producción|

> Importante mencionar que existirán variables que por su naturaleza de definición es recomendable incluir el ecosistema (ambiente) de ejecución, como sufijo en el nombre de la variable a definir. Deben ser definidos en cada uno explicitamente por lo que se recomienda implementar una nomenclatura estándar por ejemplo:

Ecosistema|Sufijo de nombre|
---|---|
Desarrollo|_DEV|
QA|_QA|
Staging|_STAG|
Producción|_PROD|

Una vez se defina con el determinado sufijo, debe ser responsabilidad del pipeline realizar las correspondientes actualizaciones para que independiente del ecosistema donde se vaya a deployar tome el valor indicado.

## Instalación de la API en Ambiente Local Dockerizado
La explicación de como instalar el aplicativo en un ambiente dockerizado parte por iniciar en un equipo (linux, Windows, Mac), el mismo debe tener los siguientes aplicativos instalados previamente:

- Docker.
- git.

Una vez ingrese deberá seguir los siguientes pasos al pie de la letra para que funcione:

1. Abrir una terminal.
2. Crear el directorio donde dejara el código fuente del proyecto y situarse en el mismo:
```bash
mkdir -p ~/microservicios
cd ~/microservicios
```
3. Clonar el repositorio del proyecto segun su prefencia, para via HTTP:
```bash
git clone https://gitlab.com/yapo_team/yotf/buyers/ms-buy-bck-go-clean-architecture.git 
```
Por SSH:
```bash
git clone git@gitlab.com:yapo_team/yotf/buyers/ms-buy-bck-go-clean-architecture.git
```
4. Ingresar credenciales de acceso al gitlab con su respectivo user y password.
5. Moverse al directorio recién descargado con el comando 'cd'.
```bash
cd ms-buy-bck-go-clean-architecture
```
6. Generar la Imagen del servidor de golang.
```bash
docker image build -t go-clean-architecture:1.0.0 . --no-cache=true
```
7. Una vez termine de instalar el paso a paso del Dockerfile con el comando anterior, validar que efectivamente se creo la imagen de forma correcta, con el comando:
```bash
docker image ls
```
8. Ejecutar un contenedor con la imagen recién creada:
```bash
docker run -d -p 8002:8002 --name server-clean -d go-clean-architecture:1.0.0
```
9. Verificar que el contenedor efectivamente esta trabajando con el comando:
```bash
docker container ls
```
10. Si se creo el Contenedor de Go y automáticamente levanta el aplicativo mostrando los logs del aplicativo.
```bash
docker logs server-clean
docker logs --follow server-clean # -- Si desea hacer un monitoreo continuo
```
11. Puede ingresar a cualquier navegador sabiendo la ip y el puerto (debería ser por default el 8002) e ingresar la siguiente URL: 
```bash
http://localhost:8002/api/v1/getCrapulo/12445
```
## Compilaciones Cross-Platform de Go
Si el proyecto/microservicio a generar se requiere en uno u otro Sistema Operativo o determinado tipo de contenedor, se da la facilidad de generar varias piezas a conveniencia, según los SSOO (Sistemas Operativos) mas comunes según [Survey de Stackoverflow 2020](https://insights.stackoverflow.com/survey/2020#technology-platforms-all-respondents5) solo realizando las siguientes acciones:

Pre-requisito: Tener instalado Git.

Una vez ingrese deberá seguir los siguientes pasos al pie de la letra para que funcione:

1. Abrir una terminal.
2. Crear el directorio donde dejara el código fuente del proyecto y situarse en el mismo:
```bash
mkdir -p ~/microservicios
cd ~/microservicios
```
3. Clonar el repositorio del proyecto segun su prefencia, para via HTTP:
```bash
git clone https://gitlab.com/yapo_team/yotf/buyers/ms-buy-bck-go-clean-architecture.git 
```
Por SSH:
```bash
git clone git@gitlab.com:yapo_team/yotf/buyers/ms-buy-bck-go-clean-architecture.git
```
4. Ingresar credenciales de acceso al gitlab con su respectivo user y password.
5. Moverse al directorio recién descargado con el comando 'cd'.
```bash
cd ms-buy-bck-go-clean-architecture
```
5. Ejecutar el comando make con la opción 'compile' 
```bash
make compile
```
> Las piezas generadas se encontraran dentro del directorio bin en el directorio principal del proyecto.

## Testing de la API
Para ejecutar pruebas unitarias en todo el proyecto, debe ejecutar el siguiente comando:
```bash
go test -v ./...
```

## Enlaces Importantes
- [Confluence oficial del Go Clean Architecture Yapo](https://yapo.atlassian.net/wiki/spaces/BT/pages/3135701416/Go+-+Clean+Architecture+con+Iris+Framework)
- [Framework Iris](https://www.iris-go.com/)
- [Repositorio Oficial de Iris](https://github.com/kataras/iris/)
- [Iris Docs](https://www.iris-go.com/docs/#/)
- [Repositorio Testify](https://github.com/stretchr/testify)
- [Repositorio godtenv](https://github.com/joho/godotenv)
- [Sitio Oficial de GORM](https://gorm.io/index.html)


## Licencia
[GPLv3.0](https://www.gnu.org/licenses/gpl-3.0.en.html)

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/8b/Copyleft.svg/220px-Copyleft.svg.png" width="25px" height="25px" style="display:inline;margin:0"> Copyleft | Yapo - 2021

## Autor
Rodrigo G. Higuera M. <<rodrigo.higuera@yapo.cl>> - Team Buyer
